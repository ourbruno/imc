app.controller('ImcController', function (
        $scope,
        $timeout,
        $location,
        $filter,
        focus,
        ImcService,
        tabelaClassificacao) {

    $scope.passos = [
        'Qual seu peso',
        'Qual sua altura',
        'Qual sua idade'
    ];

    $scope.passo = 0;
    $scope.titulo_1 = 'Calcule o seu';
    $scope.titulo_2 = 'IMC';
    $scope.tabela = tabelaClassificacao;

    $timeout(function () {
        avancarProgresso(0);
    }, 300);


    $scope.avancarPasso = function (passo, formulario) {
        console.log(formulario[passo]);
        if (verificarCampo(passo, formulario)) {
            passo++;
            if (passo < 3) {
                mudarBackground(passo);
                avancarProgresso(passo);
            } else {
                console.log(formulario);
                var peso = formulario[0];
                var altura = formulario[1];
                var idade = formulario[2] * 100; //Correção do bug da máscara
                var imc = ImcService.calcularImc(peso, altura);
                var classificacao = ImcService.retornarClassificacao(imc, idade);
                if (classificacao == 'Peso ideal') {
                    $scope.mensagem = 'Parabêns, você está no PESO IDEAL :)';
                } else {
                    $scope.mensagem = classificacao + '. Ops, está na hora de rever os seus hábitos :/';
                }
                $scope.titulo_1 = 'Seu IMC é';
                $scope.titulo_2 = $filter('number')(imc, 1);
                $location.path('/resultado');
            }
        }
    };

    $scope.iniciarCalculo = function () {
        $location.path('/');
    };

    function verificarCampo(passo, formulario) {
        $scope.erro = '';
        if (formulario[passo] == 0) {
            $scope.erro = 'Digite um valor maior que zero.';
            focus('formulario[' + passo + ']');
            return false;
        } else {
            return true;
        }
    }

    function mudarBackground(passo) {
        $scope.estilo = {'background-color': '#322d9c'};
        $timeout(function () {
            $scope.estilo = {'background-color': '#433cd1'};
            focus('formulario[' + passo + ']');
        }, 600);
    }

    function avancarProgresso(passo) {
        $scope.passo = passo;
        $scope.progresso = ((passo + 1) * 33.33) + '%';
    }

});