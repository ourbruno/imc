app.directive('myFocus', function () {
    return function (scope, element, attrs) {
        scope.$on('myFocus', function (e, name) {
            if (name === attrs.myFocus) {
                element[0].focus();
            }
        });
    };
});
app.directive('myEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {
                element[0].blur();
                scope.$apply(function () {
                    scope.$eval(attrs.myEnter);
                });
                event.preventDefault();
            }
        });
    };
});