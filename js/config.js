app.config(function ($routeProvider) {

    $routeProvider
            .when('/', {
                templateUrl: 'views/formulario.html',
            })
            .when('/resultado', {
                templateUrl: 'views/resultado.html',
            })
            .otherwise({redirectTo: '/'});

});

app.constant('tabelaClassificacao', [
    {idade: 20, imc: 18.5, faixa: '<18,5', classificacao: 'Baixo peso'},
    {idade: 20, imc: 25, faixa: '18,5 a 24,9', classificacao: 'Peso ideal'},
    {idade: 20, imc: 30, faixa: '25 a 29,9', classificacao: 'Excesso de peso'},
    {idade: 20, imc: null, faixa: '>30', classificacao: 'Obesidade'},
    {idade: 65, imc: 18.5, faixa: '<18,5', classificacao: 'Baixo peso'},
    {idade: 65, imc: 25, faixa: '18,5 a 24,9', classificacao: 'Peso ideal'},
    {idade: 65, imc: 30, faixa: '25 a 29,9', classificacao: 'Pré-obesidade'},
    {idade: 65, imc: 35, faixa: '30 a 34,9', classificacao: 'Obesidade, grau I'},
    {idade: 65, imc: 40, faixa: '35 a 39,9', classificacao: 'Obesidade, grau II'},
    {idade: 65, imc: null, faixa: '>=40', classificacao: 'Obesidade mórbida'},
    {idade: null, imc: 22, faixa: '<22', classificacao: 'Desnutrição'},
    {idade: null, imc: 24, faixa: '22 a 23,9', classificacao: 'Risco de desnutrição'},
    {idade: null, imc: 27, faixa: '24 a 26,9', classificacao: 'Peso ideal'},
    {idade: null, imc: 32, faixa: '27 a 32', classificacao: 'Pré-obesidade'},
    {idade: null, imc: null, faixa: '>32', classificacao: 'Obesidade'},
]);