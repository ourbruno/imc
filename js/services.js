app.factory('ImcService', function (tabelaClassificacao) {
    var _calcularImc = function (peso, altura) {
        return peso / (altura * altura);
    };

    var _retornarClassificacao = function (imc, idade) {
        var classificacao;
        tabelaClassificacao.forEach(function (registro) {
            if (!classificacao) {
                if (idade <= registro.idade || registro.idade === null) {
                    if (imc < registro.imc || registro.imc === null) {
                        classificacao = registro.classificacao;
                    }
                }
            }
        });
        return classificacao;
    };

    return {
        calcularImc: _calcularImc,
        retornarClassificacao: _retornarClassificacao
    };
});

app.factory('focus', function ($rootScope, $timeout) {
    return function (name) {
        $timeout(function () {
            $rootScope.$broadcast('myFocus', name);
        });
    };
});